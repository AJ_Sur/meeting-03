var CondicionUno = (1=="1"); //true
var CondicionDos = (1 === "1");
var Condicion3 = (0||1);
var Condicion4 = (true && 1);
var Condicion5 = (1-1) && (2+2);
var Condicion6 = (1-1) || (2+2);

console.log(CondicionUno); //True
console.log(CondicionDos); //False
console.log(Condicion3); //1
console.log(Condicion4); //1
console.log(Condicion5); //0
console.log(Condicion6); //4

